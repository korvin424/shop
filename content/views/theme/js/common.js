$(document).ready(function(){
    // Make sure to call jcider on the wrapper element
	//session_start();

	$('#confirm-button-next').click(function(e){

		var order_fio = $('#order_fio').val();
		var order_email = $('#order_email').val();
		var order_phone = $('#order_phone').val();
		order_phone = parseInt(order_phone);

		var order_address = $('#order_address').val();

	if(!$('.order_delivery').is(':checked')){
		$('.label_delivery').css('color','#e07b7b');
		send_order_delivery = '0';
	} else{ $('.label_delivery').css('color','black'); send_order_delivery = '1';}

		if(order_fio == '' || order_fio.length > 50){
			$('#order_fio').css('border','solid #fdb6b6');
			send_order_fio = '0';
		} else{ $('#order_fio').css('border','solid #dbdbdb'); send_order_fio = '1';}

		if(order_email == '' || order_email.length > 50){
			$('#order_email').css('border','solid #fdb6b6');
			send_order_email = '0';
		} else{ $('#order_email').css('border','solid #dbdbdb'); send_order_email = '1';}

		if(order_phone == '' || order_phone.length > 15 || isNaN(order_phone)){
			$('#order_phone').css('border','solid #fdb6b6');
			send_order_phone = '0';
		} else{ $('#order_phone').css('border','solid #dbdbdb'); send_order_phone = '1';}

		if(order_address == '' || order_address.length > 50){
			$('#order_address').css('border','solid #fdb6b6');
			send_order_address = '0';
		} else{ $('#order_address').css('border','solid #dbdbdb'); send_order_address = '1';}

		if(send_order_delivery == '1' && send_order_fio == '1' && send_order_email == '1' && send_order_address == '1' && send_order_phone == '1'){
			return true;
		}
		e.preventDefault(); // send false

		});

	function loadcart(){
		$.ajax({
			type: 'POST',
			url: '/include/loadcart.php',
			dataType: 'html',
			cache: false,
			success: function(){
				if (data == '0')
				{
					$('#block-basket > a').html('Корзина пуста');
				} else
				{
					$('#block-basket > a').html('data');
				}
			}
		})
	}
function group_numerals(intprice){
	var result_total = String(intprice);
	var lenstr = result_total.length;

	switch(lenstr) {
		case 4: {
			groupprice = result_total.substring(0,1)+ ' ' + result_total.substring(1,4);
			break;
		}
		case 5: {
			groupprice = result_total.substring(0,2) +" "+ result_total.substring(2,5);
			break;
		}
		case 6: {
			groupprice = result_total.substring(0, 3) + ' ' + result_total.substring(3,6);
			break;
		}
		case 7: {
			groupprice = result_total.substring(0, 1) + ' ' + result_total.substring(1,3) + ' ' + result_total.substring(4,7);
			break;
		}
		default: {
			groupprice = result_total;
			break;
		}
	}
	return groupprice;
}
	function itog_price() {

		$.ajax({
			type: 'POST',
			url: '/content/models/handlers/itog-price.php',
			cache: false,
			success: function(data){
				$('.itog_price > strong').html(data);
			}
		});
	}

	$('.count_minus').click(function(){

		var pid = $(this).attr('pid');

		$.ajax({
			type: 'POST',
			url: '/content/models/handlers/count-minus.php',
			data: 'id='+pid,
			dataType: 'html',
			cache: false,
			success: function(data){

				$('#input-id'+pid).val(data);
				//loadcart();

				var priceproduct = $('#tovar'+pid+' > p').attr('price');

				result_total = Number(priceproduct) * Number(data);

				$('#tovar'+pid+' > p').html(group_numerals(result_total)+ 'грн');
				$('#tovar'+pid+' > .span-count').html(data);

				itog_price();
			}

		})
	});
	$('.count-input').keypress(function(e){
		if(e.keyCode==13)

		{
			var pid = $(this).attr('pid');
			var incount = $('#input-id'+pid).val();

			$.ajax({
				type: 'POST',
				url: '/content/models/handlers/count-input.php',
				data: 'id='+pid+'&count='+incount,
				dataType: 'html',
				cache: false,
				success: function(data){
					$('#input-id'+pid).val(data);
					//loadcart();

					var priceproduct = $('#tovar'+pid+' > p').attr('price');
					result_total = Number(priceproduct) * Number(data);
					$('#tovar'+pid+' > p').html(group_numerals(result_total)+ 'грн');
					$('#tovar'+pid+' > .span-count').html(data);

					itog_price();
				}

			})}

	});
	$('.count_plus').click(function(){
		var pid = $(this).attr('pid');
		$.ajax({
			type: 'POST',
			url: '/content/models/handlers/count-plus.php',
			data: 'id='+pid,
			dataType: 'html',
			cache: false,
			success: function(data){
				$('#input-id'+pid).val(data);
				//loadcart();

				var priceproduct = $('#tovar'+pid+' > p').attr('price');
				result_total = Number(priceproduct) * Number(data);
				$('#tovar'+pid+' > p').html(group_numerals(result_total)+ 'грн');
				$('#tovar'+pid+' > .span-count').html(data);

				itog_price();
			}

		})
	});


 });