<footer id="colophon" class="site-footer" role="contentinfo">




    <div class="bottom-line clearfix">
        <div class="line-wrap">
            <div class="site-info">
                <a class="site-branding logo" rel="home" href="http://localhost/shop/">


                </a><!-- .site-branding -->

                <p class="copyright"> Copyright 2015 © Sunbury Cedar All Rights Reserved.</p>

            </div><!-- .site-info -->

        </div><!-- .line-wrap -->

    </div><!-- .bottom-line -->
</footer><!-- #colophon -->
</div><!-- #page -->
<!--bower:js-->



<script src="content/views/theme/libs/jquery/dist/1-jquery.js"></script>
<script src="content/views/theme/libs/bootstrap/dist/js/bootstrap.js"></script>
<script src="content/views/theme/libs/equalHeights/dist/js/equalheights.js"></script>
<script src="content/views/theme/libs/slicknav/dist/jquery.slicknav.js"></script>
<script src="content/views/theme/libs/flexnav/dist/js/jquery.flexnav.min.js"></script>
<script src="content/views/theme/libs/owl.carousel/dist/owl.carousel.js"></script>
<script src="content/views/theme/libs/navgoco/dist/js/jquery.navgoco.js"></script>
<!--endbower-->
<script src="content/views/theme/js/common.js"></script>

</body>


</html>
