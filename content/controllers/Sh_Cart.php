<?php
include "content/models/Total.php";
include "content/models/Delete.php";


$Total = new Total();
$Select = new Select('sh_cart, sh_products');
$DEL = new Delete(sh_cart);
$id = $Total->clear_str($_GET['id']);
$action = $Total->clear_str($_GET['action']);
switch($action)
{
    case 'clear':
        $params = ['ct_ip' => $_SERVER['REMOTE_ADDR']];
        $clear = $DEL->DelWidthParametrs($params);

        break;

    case 'delete':
        $params = ['ct_id' => $id, 'ct_ip' => $_SERVER['REMOTE_ADDR']];
        $delete = $DEL->DelWidthParametrs($params);

        break;
}

if(isset($_POST['submitdata'])){
    $_SESSION['order_delivery'] = $_POST['order_delivery'];
    $_SESSION['order_fio'] = $_POST['order_fio'];
    $_SESSION['order_email'] = $_POST['order_email'];
    $_SESSION['order_phone'] = $_POST['order_phone'];
    $_SESSION['order_address'] = $_POST['order_address'];
    $_SESSION['order_note'] = $_POST['order_note'];

    header('Location: cart?action=completion');
} ?>

