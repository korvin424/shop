<div class="box-inner">
            <div data-original-title="" class="box-header well">
                <h2><i class="glyphicon glyphicon-edit"></i> Добавление категории</h2>

                
            </div>
            <div class="box-content">
                <form role="form" method="post" action="">
                  <div class="form-group">
                        <label >Название категории</label>
                        <input type="text" placeholder="Название категории" class="form-control" name="name">

                       
                    </div>
                    <div class="form-group">
                        <label >Заголовок</label>
                        <input type="text" placeholder="Заголовок" name="title"  class="form-control">
                    </div>
                    <div class="form-group">
                        <label >Описание</label>
                        <textarea name="description"  placeholder="Описание" cols="30" class="form-control" rows="10"></textarea>

                    </div>
                   <div class="form-group">
                             <label >Статус</label>
                         <select name="status" class="form-control" >
                      <option value="1">Активно</option>
                      <option value="0">Отключено</option>

                  </select>
                    </div>
                    <div class="form-group">
                        <label >Изображение</label>
                        <input type="text" placeholder="url" name="img"  class="form-control">
                    </div>
                    <div class="form-group">
                        <label >Наследует от</label>
                        <select  class="form-control" name="type" >
                            <option selected disabled>Наследует от</option>
                            <?php for($i=0 ; $i<count($addCategories); $i++){
                                if($addCategories[$i]['name'] == $addCategories[$i]['type']) {?>
                                 <option value="<?php echo $addCategories[$i]['type']; ?>"><?php echo $addCategories[$i]['type']; ?></option>
                            <?php } }?>



                        </select>

                    </div>
                  <br>
                    <button class="btn btn-default" type="submit" name="submit">Добавить</button>
                </form>

            </div>
        </div>