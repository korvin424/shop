<div class="box col-md-12">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="glyphicon glyphicon-th">Grid 12</i>
			</h2>
			<div class="box-icon">
				<a href="#" class="btn btn-settings btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
				<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
				<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
			</div>
		</div>

		<div class="box-content">
			<div class="row">
				<div class="col-md-4"><a href="?page=Pages"><h6>Страницы</h6></a></div>
				<div class="col-md-4"><a href="?page=Categories"><h6>Категории</h6></a></div>
				<div class="col-md-4"><a href="?page=Settings"><h6>Настройки</h6></a></div>
			</div>
		</div>
	</div>
</div>