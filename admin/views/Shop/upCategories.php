<div class="box-inner">
    <div data-original-title="" class="box-header well">
        <h2><i class="glyphicon glyphicon-edit"></i> Изменение категории</h2>


    </div>
    <div class="box-content">
        <form role="form" method="post" action="">
            <div class="form-group">
                <label >Название категории</label>
                <input type="text" value="<?php echo $name; ?>" placeholder="Название категории" class="form-control" name="name">


            </div>
            <div class="form-group">
                <label >Заголовок</label>
                <input type="text" value="<?php echo $title ?>" placeholder="Заголовок" name="title"  class="form-control">
            </div>
            <div class="form-group">
                <label >Описание</label>
                <textarea name="description"   placeholder="Описание" cols="30" class="form-control" rows="10"><?php echo $description; ?></textarea>

            </div>

            <div class="form-group">
                <label >Изображение</label>
                <input type="text" placeholder="url" name="img" value="<?php echo $img; ?>"  class="form-control">
            </div>
            <div class="form-group">
                <label >Наследует от</label>
                <select  class="form-control" name="type" >

                    <?php for($i=0 ; $i<count($upCategories); $i++){
                        if($upCategories[$i]['name'] == $upCategories[$i]['type']) {

                            if(strtolower($type) == strtolower($upCategories[$i]['type'])){

                                if(strtolower($type) == strtolower($name)){  ?>
                                    <option selected value="">Не наследует</option>
                               <?php } else{ ?>
                                    <option selected value="<?php echo $upCategories[$i]['type']; ?>"><?php echo $upCategories[$i]['type']; ?></option>
                                    <option value="">Не наследует</option>

                               <?php }?>

                            <?php } else { ?>
                                 <option value="<?php echo $upCategories[$i]['type']; ?>"><?php echo $upCategories[$i]['type']; ?></option>
                                <?php
                            }
                        }
                    }
                    ?>



                </select>

            </div>
            <br>
            <button class="btn btn-default" type="submit" name="edit">Edit</button>
        </form>

    </div>
</div>