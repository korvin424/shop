<div class="box-inner">
    <div data-original-title="" class="box-header well">
        <h2><i class="glyphicon glyphicon-edit"></i> Добавление нового товара</h2>


    </div>
    <div class="box-content">
        <form role="form" method="post" action="">
            <div class="form-group">
                <label >Название товара</label>
                <input type="text" placeholder="Название товара" value="<?php echo $name; ?>" class="form-control" name="name">


            </div>
            <div class="form-group">
                <label >Цена</label>
                <input type="text" placeholder="Цена" name="price" value="<?php echo $price; ?>"  class="form-control">
            </div>
            <div class="form-group">
                <label >фото</label>
                <input type="text" placeholder="фото" value="<?php echo $img; ?>" class="form-control" name="image">


            </div>
            <div class="form-group">
                <label >Бренд товара</label>
                <select  class="form-control" name="category_id" >


                    <?php for($i=0 ; $i<count($incats); $i++){
                        if($incats[$i]['id'] == $category_id) { ?>
                            <option selected value="<?php echo $incats[$i]['id']; ?>"><?php echo $incats[$i]['name']; ?></option>
                        <?php continue; }?>
                       <?php if($incats[$i]['name'] != $incats[$i]['type'] ){ ?>
                            <option value="<?php echo $incats[$i]['id']; ?>"><?php echo $incats[$i]['name']; ?></option>
                        <?php }
                        if($incats[$i]['type'] == 'Without type'){ ?>
                            <option value="<?php echo $incats[$i]['id']; ?>"><?php echo $incats[$i]['name']; ?></option>
                        <?php } ?>

                    <?php } ?>



                </select>

            </div>




            <div class="form-group">
                <label >Тип товара</label>
                <select  class="form-control" name="type" >


                    <?php for($i=0 ; $i<count($incats); $i++){

                        if($incats[$i]['name'] == $incats[$i]['type'] ){
                            if(strtolower($type) == strtolower($incats[$i]['name'])){
                                echo "<option selected value='{$incats[$i]['type']}'>{$incats[$i]['type']}</option>";
                            } else{
                                echo "<option value='{$incats[$i]['type']}'>{$incats[$i]['name']}</option>";
                            } ?>

                        <?php }

                    } ?>

                </select>
            </div>

            <div class="form-group">
                <label >Описание товара</label>
                <textarea cols="30" rows="10"  placeholder="Описание" name="description"  class="form-control"><?php echo $description; ?></textarea>
            </div>

            <div class="form-group">
                <label >Статус</label>
                <select name="status" class="form-control" >
                    <?php if($status) {
                        echo '
                    <option selected value="1">Активно</option>
                    <option value="0">Отключено</option> ';
                    } else { echo '
    <option value="1">Активно</option>
                    <option selected value="0">Отключено</option> ';

                    }


                    ?>

                </select>
            </div>

            <br>
            <button class="btn btn-default" type="submit" name="edit">Изменить</button>
        </form>

    </div>
</div>