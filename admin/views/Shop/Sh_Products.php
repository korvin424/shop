<a href="?page=Sh_Products&action=add" class="btn btn-success">Добавить</a>
<br><br>
<table class='table table-striped table-bordered bootstrap-datatable datatable responsive'>
    <thead>
    <tr>
        <th>Название</th>
        <th>Цена</th>
        <th>Изображение</th>
        <th>Категория товара</th>
        <th>Тип товара</th>
        <th>Статус</th>
        <th>Действие</th>

    </tr>
    </thead>
    <tbody>
    <?php 
    	for($i=0; $i<count($Sh_Products); $i++) {

            if(!empty($Sh_Products[$i]['category_id'])){
               $category_id = $Sh_Products[$i]['category_id'];
                for($k=0; $k<count($Sh_Categories); $k++){
                    if($category_id == $Sh_Categories[$k]['id']){
                        $category_name = $Sh_Categories[$k]['name'];
                    }
                }
            } else{
                $category_name = '';
            }
    		if($Sh_Products[$i]['status'] == 1) {
    			$status= "<span class='label-success label label-default'>Active</span>";
    		}else {
    			$status = "<span class='label-danger label label-default'>Off</span>";
    		}
    		echo "
 <tr>
        <td>{$Sh_Products[$i]['name']}</td>
       <td>{$Sh_Products[$i]['price']}</td>
       <td> <img height='70' src='../content/views/theme/img/{$Sh_Products[$i]["image"]}' alt='logo'> </td>
        <td>{$category_name}</td>
        <th>{$Sh_Products[$i]['type']}</th>
        <td class='center'>
             $status
        </td>

        <td class='center'>
         
            <a class='btn btn-info' href='?page=Sh_products&action=update&id={$Sh_Products[$i]['id']}'>
                <i class='glyphicon glyphicon-edit icon-white'></i>
                Edit
            </a>
            <a class='btn btn-danger' href='?page=Sh_products&action=delete&id={$Sh_Products[$i]['id']}'>
                <i class='glyphicon glyphicon-trash icon-white'></i>
                Delete
            </a>
        </td>
    </tr>
    ";

    	}
    ?>
   
   </tbody>
    </table>

<pre>
<?php 

print_r($Sh_Products);

?>
</pre>