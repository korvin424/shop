
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>clean-shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="/admin/template/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="/admin/template/css/charisma-app.css" rel="stylesheet">
    <link href='/admin/template/css/jquery.noty.css' rel='stylesheet'>
    <link href='/admin/template/css/noty_theme_default.css' rel='stylesheet'>
    <link href='/admin/template/css/elfinder.min.css' rel='stylesheet'>
    <link href='/admin/template/css/elfinder.theme.css' rel='stylesheet'>
    <link href='/admin/template/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='/admin/template/css/uploadify.css' rel='stylesheet'>
    <link href='/admin/template/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"> <img alt="Logo image" src="img/logo20.png" class="hidden-xs"/>
                <span></span>
            </a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <form method="POST">
                    <button class="btn btn-default dropdown-toggle" type="submit" name="out" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Выход</span>
                        <span class="caret"></span>
                    </button>
                </form>

               <!-- <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>-->
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
           <!-- <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>-->
            <!-- theme selector ends -->

          <!--  <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="http:// /*echo $_SERVER['SERVER_NAME']; *//index.php"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
                <li>
                    <form class="navbar-search pull-left">
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                </li>
            </ul>-->

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Меню</li>
                        <li><a class="ajax-link" href="/"><i class="glyphicon glyphicon-home"></i><span> Главная</span></a>
                        </li>
                        <li><a class="ajax-link" href="?page=Pages"><i class="glyphicon glyphicon-eye-open"></i><span> Страницы</span></a>
                        </li>
                        <li><a class="ajax-link" href="form.html"><i class="glyphicon glyphicon-edit"></i><span> Формы</span></a></li>

                        <li><a class="ajax-link" href="?page=Menu"><i class="glyphicon glyphicon-edit"></i><span> Меню</span></a></li>
                        <li><a class="ajax-link" href="?page=Categories"><i class="glyphicon glyphicon-list-alt"></i><span> Категории</span></a>
                        </li>

                        </li>
                        <li><a class="ajax-link" href="?page=Settings"><i class="glyphicon glyphicon-font"></i><span> Настройки</span></a>
                        </li>
                       <li><a href="">Магазин</a>

<ul>
<li><a class="ajax-link" href="?page=Sh_Categories">Категории товаров</a></li>
<li><a class="ajax-link" href="?page=Sh_Products">Товары</a></li>
<li><a class="ajax-link" href="#">Корзина</a></li>
<li><a class="ajax-link" href="#">Оформление заказа</a></li>
</ul>
                       </li>

                       
                    </ul>

                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
 <?php if(isset($_GET['page'])){
   /* echo file_exist("controllers'.$_GET['page'].'.php'");*/
    include '/controllers/'.$_GET['page'].'.php';
} else {
    include "views/mainpanel.php";
}

 ?>
    </div>

    <footer class="row">

    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- data table plugin -->
<script src='/admin/template/js/jquery.dataTables.min.js'></script>

<script src="/admin/template/js/jquery.noty.js"></script>

<script src="/admin/template/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="/admin/template/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="/admin/template/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="/admin/template/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="/admin/template/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="/admin/template/js/charisma.js"></script>


</body>
</html>
