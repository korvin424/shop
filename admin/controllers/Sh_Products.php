<?php


include "models/Database.php";
$table = 'sh_products';
$tablecat = 'sh_categories';
if(isset($_GET['action'])){
	if($_GET['action'] == 'add') {
		include "models/Select.php";
		$catdata = new Select($tablecat);
		$addProducts = $catdata->getAllData();
		include "views/Shop/addProducts.php";

		if(isset($_POST['submit'])){ //Проверка на нажитие и на имя кнопки
			$data['description'] = $_POST['description'];
			$data['name'] = $_POST['name'];
			$data['price'] = $_POST['price'];
			$data['image'] = $_POST['image'];
			$data['status'] = $_POST['status'];

			if(!empty($_POST['category_id'])){
				$data['category_id'] = $_POST['category_id'];
				if(empty($_POST['type'])){
					$category_id = $_POST['category_id'];
					for($i=0; $i<count($addProducts); $i++){
						if($addProducts[$i]['id'] == $category_id){
							$data['type'] = $addProducts[$i]['type'];
						}
					}
				} else{
					$data['type'] = $_POST['type'];
				}
			}else{
				for($i=0 ; $i<count($addProducts); $i++){
                   if($addProducts[$i]['type'] == 'Without type') {
						$data['category_id'] = $addProducts[$i]['id'];
					   if(empty($_POST['type'])){
						   $category_id = $addProducts[$i]['id'];
						   $data['type'] = $addProducts[$i]['type'];
					   } else{
						   $data['type'] = $_POST['type'];
					   }
				   }
				}
			}
			include "models/Insert.php";
			$insertData = new Insert($table, $data);


			echo '<script type="text/javascript">';
			echo 'window.location.href="/admin/?page=Sh_products"';
			echo '</script>';
		}

	}
	if($_GET['action']=="delete"){
		include "models/Delete.php";
		$delData = new Delete($table, $_GET['id']);
		echo '<script type="text/javascript">';
		echo 'window.location.href="/admin/?page=Sh_products"';
		echo '</script>';
	}
	if($_GET['action']=="update"){
		include "models/Select.php";
		$sel = new Select($table);
		$sel2 = new Select($tablecat);
		$id = $_GET['id'];
		$upProducts = $sel->getAllData();
		$incats= $sel2->getAllData();
		for($i=0;$i<count($upProducts);$i++){
			if($_GET['id'] == $upProducts[$i]['id'] ){
				$id = $upProducts[$i]['id'];
				$price = $upProducts[$i]['price'];
				$img = $upProducts[$i]['image'];
				$name = $upProducts[$i]['name'];
				$type = $upProducts[$i]['type'];
				$description = $upProducts[$i]['description'];
				$status = $upProducts[$i]['status'];
				$category_id = $upProducts[$i]['category_id'];

			}
		}
		include "views/Shop/upProducts.php";

		if(isset($_POST['edit'])){ //Проверка на нажитие и на имя кнопки
			echo '<h1>' . $_POST['type'] .  '</h1>';
			$data['description'] = $_POST['description'];
			$data['name'] = $_POST['name'];
			$data['price'] = $_POST['price'];
			$data['status'] = $_POST['status'];
			$data['category_id'] = $_POST['category_id'];
			$data['image'] = $_POST['image'];
			if(!empty($_POST['category_id'])){
				$data['category_id'] = $_POST['category_id'];
				if(empty($_POST['type'])){
					$category_id = $_POST['category_id'];
					for($i=0; $i<count($addProducts); $i++){
						if($addProducts[$i]['id'] == $category_id){
							$data['type'] = $addProducts[$i]['type'];
						}
					}
				} else{
					$data['type'] = $_POST['type'];
				}
			}else{
				for($i=0 ; $i<count($addProducts); $i++){
					if($addProducts[$i]['type'] == 'Without type') {
						$data['category_id'] = $addProducts[$i]['id'];
						if(empty($_POST['type'])){
							$category_id = $addProducts[$i]['id'];
							$data['type'] = $addProducts[$i]['type'];
						} else{
							$data['type'] = $_POST['type'];
						}
					}
				}
			}
			$params = ['name' => $data['name'], 'description' => $data['description'], 'price' => $data['price'], 'status' => $data['status'], 'category_id' => $data['category_id'], 'image' => $data['image'], 'type'=> $data['type']];
			include "models/Update.php";
			$UpdateData = new Update($table);
			$update = $UpdateData->UpdateData($params, $id);

			echo '<script type="text/javascript">';
			echo 'window.location.href="/admin/?page=Sh_products"';
			echo '</script>';


		}
	}
} else {
		include "models/Select.php";
		$data = new Select($table);
		$cat = new Select($tablecat);
		$Sh_Categories = $cat->getAllData();
		$Sh_Products = $data->getAllData();
		include "views/Shop/Sh_Products.php";
	}
