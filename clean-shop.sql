-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 18, 2016 at 01:45 AM
-- Server version: 5.5.41-log
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clean-shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `name`, `status`, `parent_id`, `content`) VALUES
(2, 'tatata', 'dfsfds', 'new', 0, 0, ''),
(3, 'оеное', 'оеное', 'пеао', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  `some-2` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `url`, `parent_id`, `some-2`) VALUES
(4, 'Главная', '/', 0, 0),
(5, 'Корзина', 'cart', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `description`, `name`, `content`, `parent_id`) VALUES
(1, '', '', 'cart', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sh_cart`
--

CREATE TABLE IF NOT EXISTS `sh_cart` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `ct_price` int(11) NOT NULL,
  `ct_count` int(11) NOT NULL,
  `ct_datatime` datetime NOT NULL,
  `ct_ip` varchar(100) NOT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `sh_cart`
--

INSERT INTO `sh_cart` (`ct_id`, `id_product`, `ct_price`, `ct_count`, `ct_datatime`, `ct_ip`) VALUES
(72, 7, 200000, 15, '2016-09-16 00:42:15', '127.0.0.1'),
(75, 8, 175000, 6, '2016-09-16 10:52:40', '127.0.0.1'),
(76, 10, 250000, 4, '2016-09-16 21:10:04', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `sh_categories`
--

CREATE TABLE IF NOT EXISTS `sh_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `img` text NOT NULL,
  `description` text NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `sh_categories`
--

INSERT INTO `sh_categories` (`id`, `name`, `title`, `img`, `description`, `type`) VALUES
(9, 'Clothers', '', '/content/views/theme/img/clothers.jpg', 'fsdfsa', 'Clothers'),
(10, 'Cars', '', '/content/views/theme/img/logo.jpg', 'fsdfsa', 'Cars'),
(11, 'Dodge', '', '/content/views/theme/img/dod.jpg', 'fsdfsa', 'cars'),
(12, 'Ford-mustang', '', '/content/views/theme/img/mustang-emblem.jpg', 'fsdfsa', 'cars'),
(13, 'T-shirts', '', '/content/views/theme/img/fifth-vector.png', 'fsdfsa', 'clothers'),
(14, 'Warm clothes', '', '/content/views/theme/img/nike.jpg', 'fsdfsa', 'clothers'),
(18, 'Food', '', '/content/views/theme/img/Limewire.png', 'fsdfsa', 'Food'),
(24, 'Without type', '', '/content/views/theme/img/Aqua-Button.png', 'fsdfsa', 'Without type'),
(36, 'common', '', '/content/views/theme/img/dashboard.ico', 'fsdfsa', 'common');

-- --------------------------------------------------------

--
-- Table structure for table `sh_products`
--

CREATE TABLE IF NOT EXISTS `sh_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `price` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `sh_products`
--

INSERT INTO `sh_products` (`id`, `name`, `price`, `description`, `image`, `status`, `category_id`, `type`) VALUES
(1, 'Product-1', 2000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt similique laborum quasi deserunt eveniet vitae deleniti, autem consequatur nisi fugit, voluptatem obcaecati odit, expedita natus neque quaerat a ea saepe. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt similique laborum quasi deserunt eveniet vitae deleniti, autem consequatur nisi fugit, voluptatem obcaecati odit, expedita natus neque quaerat a ea saepe.\r\n								\r\n								', 'first-vector.png', 1, 24, 'clothers'),
(2, 'Product-2', 3000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa animi corporis sapiente voluptas, minus beatae quasi sequi mollitia, minima eum! Consectetur doloremque veritatis porro enim quaerat nihil odio, voluptatibus cumque.\r\n    ', 'second-vector.png', 1, 14, 'clothers'),
(3, 'Product-3', 1000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa animi corporis sapiente voluptas, minus beatae quasi sequi mollitia, minima eum! Consectetur doloremque veritatis porro enim quaerat nihil odio, voluptatibus cumque.\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa animi corporis sapiente voluptas, minus beatae quasi sequi mollitia, minima eum! Consectetur doloremque veritatis porro enim quaerat nihil odio, voluptatibus cumque.\r\n    ', 'third-vector.png', 1, 13, 'clothers'),
(4, 'Product-4', 500, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, accusamus iusto! Recusandae dignissimos porro ipsam dolorem maiores? Voluptatibus ex, dolores laborum reprehenderit praesentium mollitia fugit, alias fuga. At est, saepe.\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, accusamus iusto! Recusandae dignissimos porro ipsam dolorem maiores? Voluptatibus ex, dolores laborum reprehenderit praesentium mollitia fugit, alias fuga. At est, saepe.\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, accusamus iusto! Recusandae dignissimos porro ipsam dolorem maiores? Voluptatibus ex, dolores laborum reprehenderit praesentium mollitia fugit, alias fuga. At est, saepe.\r\n    ', 'second-vector.png', 0, 24, 'clothers'),
(5, 'product-5', 1200, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eligendi libero minus deserunt culpa tempora, nulla earum? Magni eos ex amet laudantium placeat, repellat, minima ab hic quas sed nam.\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eligendi libero minus deserunt culpa tempora, nulla earum? Magni eos ex amet laudantium placeat, repellat, minima ab hic quas sed nam.\r\n    ', 'four-vector.png', 1, 14, 'clothers'),
(6, 'Product-6', 1500, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi eveniet distinctio molestias sequi adipisci, dolorum mollitia tempora quas eum cumque in totam quam natus earum sit harum facere iusto porro.\r\n    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi eveniet distinctio molestias sequi adipisci, dolorum mollitia tempora quas eum cumque in totam quam natus earum sit harum facere iusto porro.\r\n    ', 'fifth-vector.png', 1, 13, 'clothers'),
(7, 'Dodge-1', 200000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.', 'dodge.jpg', 1, 11, 'cars'),
(8, 'dodge-2', 175000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.', 'dodge2.jpg', 1, 11, 'cars'),
(9, 'Ford Mustang-1', 225000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.', 'ford mustang.JPG', 1, 12, 'cars'),
(10, 'Ford Mustang-2', 250000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, repudiandae architecto earum assumenda aspernatur temporibus recusandae quibusdam nam vero eum aliquid ipsa voluptatum debitis, neque cupiditate velit placeat doloremque, iusto.', 'shelby.jpg', 1, 12, 'cars'),
(24, 'What', 3000, 'loremloremloremloremloremloremloremloremloremloremloremloremlorem', 'souris .ico', 1, 24, 'Without type'),
(35, 'what is', 100, '', 'stylo.ico', 1, 24, 'Without type'),
(36, 'new product', 5000, 'new product', 'lp2.jpg', 1, 24, 'Cars');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'admin', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `users_in`
--

CREATE TABLE IF NOT EXISTS `users_in` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(11) NOT NULL,
  `login` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `brauzer` varchar(50) NOT NULL,
  `count` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
